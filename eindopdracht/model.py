# import the necessary libraries
from collections import Counter
from copy import deepcopy
from random import uniform, shuffle
from math import sqrt, exp
from math import tanh as math_tanh
import numpy as np


# activation functions
def sign(a):
    if a < 0:
        return -1
    elif a > 0:
        return 1
    else:
        return 0


def linear(a):
    return a


def tanh(x):
    return math_tanh(x)


def softsign(a):
    return a / (1 + abs(a))


def sigmoid(x):
    try:
        return 1 / (1 + exp(-x))
    except OverflowError:
        return 0


def softplus(x):
    try:
        return pseudo_ln(1 + exp(x))
    except OverflowError:
        return 0


def relu(x):
    return max(0, x)


# function for log function to overcome the
# problem of giving a negative value to the log function
def pseudo_ln(x):
    e = 0.000001
    if x >= e:
        return np.log(x)
    else:
        return np.log(e) + ((x - e) / e)


# loss functions
def mean_squared_error(y_hat, y):
    return (y_hat - y) ** 2


def mean_absolute_error(y_hat, y):
    return abs(y_hat - y)


def categorical_crossentropy(yhat, y):
    return -y * pseudo_ln(yhat)


def binary_crossentropy(yhat, y):
    return (-y * pseudo_ln(yhat)) - ((1 - y) * pseudo_ln(1 - yhat))


def hinge(yhat, y):
    return max(1 - yhat * y, 0)


# derivative function
def derivative(function, delta=0.001):
    def wrapper_derivative(x, *args):
        return (function(x + delta, *args) - function(x - delta, *args)) / (2 * delta)

    wrapper_derivative.__name__ = function.__name__ + '’'
    wrapper_derivative.__qualname__ = function.__qualname__ + '’'
    return wrapper_derivative


"""
Class that makes a perceptron that can
build a model for data with binary classes.
It checks the predicted outcome with the real
outcome and updates the bias and weights so the
instances will be correctly classified.
"""
class Perceptron():

    def __repr__(self):
        text = f'Perceptron(dim={self.dim})'
        return text

    def __init__(self, dim):
        # number of attributes in the data
        self.dim = dim
        # initial bias
        self.bias = 0.0
        # initial weights
        self.weights = [0.0] * dim

    def predict(self, xs):
        # list for the predicted outcomes
        yhats = []

        # calculate the predicted outcomes for every instance
        for i in range(len(xs)):
            a = self.bias
            # loop through every attribute
            for d in range(self.dim):
                # calculate the predicted outcome
                a += xs[i][d] * self.weights[d]

            # if the outcome is > 0, predicted outcome is 1
            if a > 0:
                yhat = 1.0
            # if the outcome is < 0, predicted outcome is -1
            elif a < 0:
                yhat = -1.0
            else:
                yhat = 0.0
            # fill the list with the predicted outcomes
            yhats.append(yhat)
        return yhats

    def partial_fit(self, xs, ys):
        # get the predicted outcomes for the instances
        yhats = self.predict(xs)

        # update the bias and weights by looping through every instance
        for x, y, yhat in zip(xs, ys, yhats):
            # the erorr for the instance
            e = yhat - y
            # update the bias
            self.bias -= e

            # update the weights for every attribute
            for i in range(self.dim):
                self.weights[i] -= e * x[i]

    def fit(self, xs, ys, epochs=0):
        # if number of epochs is not given/0
        # keep updating until all predictions are correct
        if epochs == 0:
            all_correct = False

            # while not all predictions are correct update the bias and weights
            while not all_correct:
                self.partial_fit(xs, ys)
                yhats = self.predict(xs)

                # if the predictions are the same as the actual outcomes
                # set the variable to true to stop updating
                if yhats == ys:
                    all_correct = True

        # update the bias and weights a number of times given the number of epochs
        else:
            for i in range(epochs):
                self.partial_fit(xs, ys)


"""
Class that performs linear regression on data.
It updates the bias and weights to build the model
as good as possible.
"""
class LinearRegression():

    def __repr__(self):
        text = f'Perceptron(dim={self.dim})'
        return text

    def __init__(self, dim):
        # number of attributes in the data
        self.dim = dim
        # initial bias
        self.bias = 0.0
        # initial weights
        self.weights = [0.0] * dim

    def predict(self, xs):
        # list for the predicted outcomes
        yhats = []

        # calculate the predicted outcomes for every instance
        for i in range(len(xs)):
            a = self.bias
            # loop through every attribute
            for d in range(self.dim):
                # calculate the predicted outcome
                a += xs[i][d] * self.weights[d]
            # add the predicted outcome to the list
            yhats.append(a)
        return yhats

    def partial_fit(self, xs, ys, alpha=0.001):
        # get the predicted outcomes for the instances
        yhats = self.predict(xs)

        # update the bias and weights by looping through every instance
        for x, y, yhat in zip(xs, ys, yhats):
            # the erorr for the instance
            e = yhat - y
            # update the bias
            self.bias -= alpha * e

            # update the weights for every attribute
            for i in range(self.dim):
                self.weights[i] -= alpha * (e * x[i])

    def fit(self, xs, ys, alpha=0.001, epochs=50):
        # update the bias and weights a number of times given the number of epochs
        for i in range(epochs):
            self.partial_fit(xs, ys, alpha)


"""
Class to build a neuron that will update biases
and weights to correctly classify given instances.
It also calculates the loss for the overall model.
"""
class Neuron():

    def __repr__(self):
        text = f'Neuron(dim={self.dim}, activation={self.activation.__name__}, loss={self.loss.__name__})'
        return text

    def __init__(self, dim, activation=linear, loss=mean_squared_error):
        # number of attributes in the data
        self.dim = dim
        # the given activation function
        self.activation = activation
        # the given loss function
        self.loss = loss
        # initial bias
        self.bias = 0
        # initial weights
        self.weights = [0.0] * dim

    def predict(self, xs):
        # list for the predicted outcomes
        yhats = []

        # calculate the predicted outcomes for every instance
        for i in range(len(xs)):
            a = self.bias
            # loop through every attribute
            for d in range(self.dim):
                # calculate the predicted outcome
                a += xs[i][d] * self.weights[d]
            # get the predicted outcome by using the given activation function
            yhat = self.activation(a)
            # add the predicted outcome to the list
            yhats.append(yhat)
        return yhats

    def partial_fit(self, xs, ys, alpha=0.001):
        # get the predicted outcomes for the instances
        yhats = self.predict(xs)

        # update the bias and weights by looping through every instance
        for x, y, yhat in zip(xs, ys, yhats):

            # get the derivatives for the loss and activation functions
            der_loss = derivative(self.loss)
            der_act = derivative(self.activation)

            # update the bias
            self.bias -= alpha * der_loss(yhat, y) * der_act(yhat)

            # loop through every attribute
            for i in range(self.dim):
                # update the weights
                self.weights[i] -= alpha * der_loss(yhat, y) * der_act(yhat) * x[i]

    def fit(self, xs, ys, alpha=0.001, epochs=40):
        # update the bias and weights a number of times given the number of epochs
        for i in range(epochs):
            self.partial_fit(xs, ys, alpha)


"""
Class for building a layer in a neural network.
It needs the number of outputs that the layer produces.
It is the basis for different child class layers
that can be part of a neural network.
"""
class Layer:
    classcounter = Counter()

    def __init__(self, outputs, *, name=None, next=None):
        # count the number of classes
        Layer.classcounter[type(self)] += 1
        if name is None:
            name = f'{type(self).__name__}_{Layer.classcounter[type(self)]}'
        self.inputs = 0
        self.outputs = outputs
        self.name = name
        # next layer
        self.next = next

    def __repr__(self):
        text = f'Layer(inputs={self.inputs}, outputs={self.outputs}, name={repr(self.name)})'
        if self.next is not None:
            text += ' + ' + repr(self.next)
        return text

    def add(self, next):
        # set next to the new added layer when there is no next layer
        if self.next is None:
            self.next = next
            # set the number of outputs equal to the number of inputs for the next layer
            next.set_inputs(self.outputs)
        # when layer has next layer, make new layer next layer for that one
        else:
            self.next.add(next)

    def __add__(self, next):
        # make add function applicable through '+' sign
        result = deepcopy(self)
        result.add(deepcopy(next))
        return result

    def __getitem__(self, index):
        # give current layer
        if index == 0 or index == self.name:
            return self

        # give layer depending on index number
        if isinstance(index, int):
            if self.next is None:
                raise IndexError('Layer index out of range')
            return self.next[index - 1]

        # give layer depending on the name
        if isinstance(index, str):
            if self.next is None:
                raise KeyError(index)
            return self.next[index]
        raise TypeError(f'Layer indices must be integers or strings, not {type(index).__name__}')

    def set_inputs(self, inputs):
        # set the inputs
        self.inputs = inputs

    def __call__(self, xs):
        raise NotImplementedError('Abstract __call__ method')


"""
Class for the inputlayer of the neural network.
The number of outputs that needs to be set is the
number of attributes present in the data.
"""
class InputLayer(Layer):

    def __repr__(self):
        text = f'InputLayer(outputs={self.outputs}, name={repr(self.name)})'
        if self.next is not None:
            text += ' + ' + repr(self.next)
        return text

    def set_inputs(self, inputs):
        # raise error when inputs are given to the input layer
        raise NotImplementedError("The input layer is the first layer in a network.")

    def __call__(self, xs, ys=None, alpha=None):
        yhats, ls, gs = self.next(xs, ys, alpha)
        return yhats, ls, gs

    def predict(self, xs):
        yhats, _, _ = self(xs)
        return yhats

    def evaluate(self, xs, ys):
        # calculate the mean loss
        _, ls, _ = self(xs, ys)
        lmean = sum(ls) / len(ls)
        return lmean

    def partial_fit(self, xs, ys, alpha=0.001, batch_size=None):
        # when no batch size is given use full batch learning
        if batch_size is None:
            batch_size = len(xs)

        # calculate the mean loss per batch
        loss = []
        for i in range(0, len(xs), batch_size):
            _, ls, _ = self(xs[i:i + batch_size], ys[i:i + batch_size], alpha)
            loss.extend(ls)
        # calculate the mean loss for all batches
        lmean = sum(loss) / len(loss)
        return lmean


    def fit(self, xs, ys, *, alpha=0.001, epochs=100, validation_data=None, batch_size=None):
        # if no validation data is given only calculate loss for given instances
        if validation_data is None:
            history = {'loss': []}
            for epoch in range(epochs):
                # shuffle the instances to pick random batches
                elements = list(zip(xs, ys))
                shuffle(elements)
                xs, ys = zip(*elements)

                # get the mean loss
                lmean = self.partial_fit(xs, ys, alpha=alpha, batch_size=batch_size)
                history['loss'].append(lmean)
        else:
            history = {'loss': [], 'val_loss': []}
            for epoch in range(epochs):
                # shuffle the instances to pick random batches
                elements = list(zip(xs, ys))
                shuffle(elements)
                xs, ys = zip(*elements)

                # get the mean loss for the trainings data
                lmean = self.partial_fit(xs, ys, alpha=alpha, batch_size=batch_size)
                history['loss'].append(lmean)

                # get the mean loss for the validation data
                val_lmean = self.evaluate(validation_data[0], validation_data[1])
                history['val_loss'].append(val_lmean)
        return history


"""
Class for dense layer which will calculate the
predicted outcome for the given instances.
"""
class Dense(Layer):

    def __init__(self, outputs, *, name=None, next=None):
        super().__init__(outputs, name=name, next=next)
        # initial bias and weights
        self.bias = [0.0] * self.outputs
        self.weights = [[]] * self.outputs

    def __repr__(self):
        text = f'Dense(outputs={self.outputs}, name={repr(self.name)})'
        if self.next is not None:
            text += ' + ' + repr(self.next)
        return text

    def set_inputs(self, inputs):
        super().set_inputs(inputs)
        # calculate the random value for the initial values of the weights
        random_value = sqrt(6.0 / (self.inputs + self.outputs))
        self.weights = [[uniform(-random_value, random_value) for i in range(self.inputs)] for o in range(self.outputs)]

    def __call__(self, xs, ys=None, alpha=None):
        hs = [[self.bias[o] + sum(self.weights[o][i] * x[i] for i in range(self.inputs))
               for o in range(self.outputs)] for x in xs]
        yhats, ls, qs = self.next(hs, ys, alpha)

        # when alpha is not given, don't let the model learn
        if alpha is None:
            gs = None
        else:
            # update the bias and weights
            for q, x in zip(qs, xs):
                for o in range(self.outputs):
                    factor = alpha * q[o]
                    self.bias[o] -= factor
                    for i in range(self.inputs):
                        self.weights[o][i] -= factor * x[i]

            #
            gs = [[sum(q[o] * self.weights[o][i] for o in range(self.outputs)) for i in range(self.inputs)] for q in qs]

        return yhats, ls, gs


"""
Class docstring
"""
class Activation(Layer):

    def __init__(self, outputs, activation=linear, name=None):
        super().__init__(outputs, name=name, next=None)
        # activation function
        self.activation = activation
        # derivative of the activation function
        self.activation_gradient = derivative(activation)

    def __repr__(self):
        super().__repr__()
        text = f'Activation(inputs={self.inputs}, outputs={self.outputs}, activation={self.activation.__name__}, name={repr(self.name)})'
        if self.next is not None:
            text += ' + ' + repr(self.next)
        return text

    def __call__(self, xs, ys=None, alpha=None):
        hs = [[self.activation(xi) for xi in x] for x in xs]
        yhats, ls, qs = self.next(hs, ys, alpha)

        # when alpha is not given, don't let the model learn
        if alpha is None:
            gs = None
        #
        else:
            gs = [[q[o] * self.activation_gradient(x[o]) for o in range(self.outputs)] for x, q in zip(xs, qs)]
        return yhats, ls, gs


"""
Softmax layer for using one hot encoding
when the data contains multiple classes.
"""
class Softmax(Layer):

    def __init__(self, outputs, name=None):
        super().__init__(outputs, name=name, next=None)

    def __repr__(self):
        super().__repr__()
        text = f'Softmax(inputs={self.inputs}, outputs={self.outputs}, name={repr(self.name)})'
        if self.next is not None:
            text += ' + ' + repr(self.next)
        return text

    def __call__(self, xs, ys=None, alpha=None):
        hs = []
        for x in xs:
            h = []
            for o in range(self.outputs):
                h.append(exp(x[o] - max(x)))
            e = []
            for i in h:
                e.append(i / sum(h))
            hs.append(e)
        yhats, ls, qs = self.next(hs, ys, alpha)
        if alpha is None:
            gs = None
        else:
            gs = [[sum(q[i] * h[i] * ((i == o) - h[o]) for i in range(self.inputs)) for o in range(self.outputs)] for
                  h, q in zip(hs, qs)]

        return yhats, ls, gs


"""
Output layer which will calculate the loss
for the model.
"""
class OutputLayer(Layer):

    def __init__(self, loss=mean_squared_error, *, name=None):
        super().__init__(0, name=name, next=next)
        # loss function
        self.loss = loss
        self.loss_gradient = derivative(loss)

    def __repr__(self):
        super().__repr__()
        text = f'OutputLayer(inputs={self.inputs}, loss={self.loss.__name__}, name={repr(self.name)})'
        return text

    def add(self, next):
        # raise error when new layer is added to last layer
        raise NotImplementedError("The output layer in a network is the last layer.")

    def __call__(self, xs, ys=None, alpha=None):
        # input of output layer are predicted values
        yhats = xs

        # if no real otucomes are given don't update the model
        if ys is None:
            ls = None
            gs = None
        else:
            # calculate the losses for the instances
            ls = [sum(self.loss(x[i], y[i]) for i in range(self.inputs)) for x, y in zip(xs, ys)]
            # when alpha is not given, don't let the model learn
            if alpha is None:
                gs = None
            else:
                gs = [[self.loss_gradient(x[i], y[i]) for i in range(self.inputs)] for x, y in zip(xs, ys)]
        return yhats, ls, gs
