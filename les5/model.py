from collections import Counter
from copy import deepcopy
from random import uniform, shuffle
from math import sqrt, exp
import math
import numpy as np
import sys

sys.path.append("../")
from les2.model import linear, sign, tanh, mean_squared_error, derivative


def softsign(a):
    return a / (1 + abs(a))


def pseudo_ln(x):
    e = 0.000001
    if x >= e:
        return np.log(x)
    else:
        return np.log(e) + ((x - e) / e)


def categorical_crossentropy(yhat, y):
    return -y * pseudo_ln(yhat)


def binary_crossentropy(yhat, y):
    return (-y * pseudo_ln(yhat)) - ((1 - y) * pseudo_ln(1 - yhat))


def sigmoid(x):
    try:
        return 1 / (1 + exp(-x))
    except OverflowError:
        return 0


def softplus(x):
    try:
        return pseudo_ln(1 + exp(x))
    except OverflowError:
        return 0


def relu(x):
    return max(0, x)


class Layer:
    classcounter = Counter()

    def __init__(self, outputs, *, name=None, next=None):
        # count the number of classes
        Layer.classcounter[type(self)] += 1
        if name is None:
            name = f'{type(self).__name__}_{Layer.classcounter[type(self)]}'
        self.inputs = 0
        self.outputs = outputs
        self.name = name
        # next layer
        self.next = next

    def __repr__(self):
        text = f'Layer(inputs={self.inputs}, outputs={self.outputs}, name={repr(self.name)})'
        if self.next is not None:
            text += ' + ' + repr(self.next)
        return text

    def add(self, next):
        # set next to the new added layer when there is no next layer
        if self.next is None:
            self.next = next
            # set the number of outputs equal to the number of inputs for the next layer
            next.set_inputs(self.outputs)
        # when layer has next layer, make new layer next layer for that one
        else:
            self.next.add(next)

    def __add__(self, next):
        # make add function applicable through + sign
        result = deepcopy(self)
        result.add(deepcopy(next))
        return result

    def __getitem__(self, index):
        # geef huidige laag terug
        if index == 0 or index == self.name:
            return self
        # give layer depending on index number
        if isinstance(index, int):
            if self.next is None:
                raise IndexError('Layer index out of range')
            return self.next[index - 1]
        # give layer depending on the name
        if isinstance(index, str):
            if self.next is None:
                raise KeyError(index)
            return self.next[index]
        raise TypeError(f'Layer indices must be integers or strings, not {type(index).__name__}')

    def set_inputs(self, inputs):
        self.inputs = inputs

    def __call__(self, xs):
        raise NotImplementedError('Abstract __call__ method')


class InputLayer(Layer):

    def __repr__(self):
        text = f'InputLayer(outputs={self.outputs}, name={repr(self.name)})'
        if self.next is not None:
            text += ' + ' + repr(self.next)
        return text

    def set_inputs(self, inputs):
        # raise error when inputs are given to the input layer
        raise NotImplementedError("The input layer is the first layer in a network.")

    def __call__(self, xs, ys=None, alpha=None):
        yhats, ls, gs = self.next(xs, ys, alpha)
        return yhats, ls, gs

    def predict(self, xs):
        yhats, _, _ = self(xs)
        return yhats

    def evaluate(self, xs, ys):
        _, ls, _ = self(xs, ys)
        lmean = sum(ls) / len(ls)
        return lmean

    def partial_fit(self, xs, ys, alpha=0.001, batch_size=None):
        if batch_size is None:
            batch_size = len(xs)

        loss = []
        for i in range(0, len(xs), batch_size):
            _, ls, _ = self(xs[i:i+batch_size], ys[i:i+batch_size], alpha)
            loss.extend(ls)
        lmean = sum(loss) / len(loss)
        return lmean


    def fit(self, xs, ys, *, alpha=0.001, epochs=100, validation_data=None, batch_size=None):
        if validation_data is None:
            history = {'loss': []}
            for epoch in range(epochs):
                elements = list(zip(xs, ys))
                shuffle(elements)
                xs, ys = zip(*elements)
                lmean = self.partial_fit(xs, ys, alpha=alpha, batch_size=batch_size)
                history['loss'].append(lmean)
        else:
            history = {'loss': [], 'val_loss': []}
            for epoch in range(epochs):
                elements = list(zip(xs, ys))
                shuffle(elements)
                xs, ys = zip(*elements)
                lmean = self.partial_fit(xs, ys, alpha=alpha, batch_size=batch_size)
                history['loss'].append(lmean)
                val_lmean = self.evaluate(validation_data[0], validation_data[1])
                history['val_loss'].append(val_lmean)
        return history


class Dense(Layer):

    def __init__(self, outputs, *, name=None, next=None):
        super().__init__(outputs, name=name, next=next)
        self.bias = [0.0] * self.outputs
        self.weights = [[]] * self.outputs

    def __repr__(self):
        text = f'Dense(outputs={self.outputs}, name={repr(self.name)})'
        if self.next is not None:
            text += ' + ' + repr(self.next)
        return text

    def set_inputs(self, inputs):
        super().set_inputs(inputs)
        # calculate the random value for the start values of the weights
        random_value = sqrt(6.0 / (self.inputs + self.outputs))
        self.weights = [[uniform(-random_value, random_value) for i in range(self.inputs)] for o in range(self.outputs)]

    def __call__(self, xs, ys=None, alpha=None):
        hs = [[self.bias[o] + sum(self.weights[o][i] * x[i] for i in range(self.inputs)) for o in range(self.outputs)]
              for x in xs]
        yhats, ls, qs = self.next(hs, ys, alpha)

        if alpha is None:
            gs = None
        else:
            for q, x in zip(qs, xs):
                for o in range(self.outputs):
                    factor = alpha * q[o]
                    self.bias[o] -= factor
                    for i in range(self.inputs):
                        self.weights[o][i] -= factor * x[i]

            gs = [[sum(q[o] * self.weights[o][i] for o in range(self.outputs)) for i in range(self.inputs)] for q in qs]

        return yhats, ls, gs


class Activation(Layer):

    def __init__(self, outputs, activation=linear, name=None):
        super().__init__(outputs, name=name, next=None)
        # activation function
        self.activation = activation
        # derivative of the activation function
        self.activation_gradient = derivative(activation)

    def __repr__(self):
        super().__repr__()
        text = f'Activation(inputs={self.inputs}, outputs={self.outputs}, activation={self.activation.__name__}, name={repr(self.name)})'
        if self.next is not None:
            text += ' + ' + repr(self.next)
        return text

    def __call__(self, xs, ys=None, alpha=None):
        hs = [[self.activation(xi) for xi in x] for x in xs]
        yhats, ls, qs = self.next(hs, ys, alpha)
        if alpha is None:
            gs = None
        else:
            gs = [[qi * self.activation_gradient(xi) for xi, qi in zip(x, q)] for x, q in zip(xs, qs)]
        return yhats, ls, gs


class Softmax(Layer):

    def __init__(self, outputs, name=None):
        super().__init__(outputs, name=name, next=None)

    def __repr__(self):
        super().__repr__()
        text = f'Softmax(inputs={self.inputs}, outputs={self.outputs}, name={repr(self.name)})'
        if self.next is not None:
            text += ' + ' + repr(self.next)
        return text

    def __call__(self, xs, ys=None, alpha=None):
        # hs = []
        # for x in xs:
        #     h = []
        #     for xi in x:
        #         h.append(math.e**(xi - max(x)))
        #     hs.append(h)
        #
        # es = []
        # for h in hs:
        #     e = []
        #     for i in h:
        #         e.append(i/sum(h))
        #     es.append(e)
        #
        # yhats, ls, qs = self.next(es, ys, alpha)
        #
        # if alpha is None:
        #     gs = None
        # else:
        #     gs = [[sum(q[o] * e[o] * ((o == i) - e[i]) for o in range(self.outputs)) for i in range(self.inputs)] for e, q in zip(es, qs)]
        hs = []
        for x in xs:
            h = []
            for o in range(self.outputs):
                h.append(math.exp(x[o]-max(x)))
            e = []
            for i in h:
                e.append(i/sum(h))
            hs.append(e)
        yhats, ls, qs = self.next(hs, ys, alpha)
        if alpha is None:
            gs = None
        else:
            gs = [[sum(q[o] * h[o] * ((o == i) - h[i]) for o in range(self.outputs)) for i in range(self.inputs)] for h, q in zip(hs, qs)]

        return yhats, ls, gs


class OutputLayer(Layer):

    def __init__(self, loss=mean_squared_error, *, name=None):
        super().__init__(0, name=name, next=next)
        # loss function
        self.loss = loss
        self.loss_gradient = derivative(loss)

    def __repr__(self):
        super().__repr__()
        text = f'OutputLayer(inputs={self.inputs}, loss={self.loss.__name__}, name={repr(self.name)})'
        return text

    def add(self, next):
        # raise error when new layer is added to last layer
        raise NotImplementedError("The output layer in a network is the last layer.")

    def __call__(self, xs, ys=None, alpha=None):
        yhats = xs
        if ys is None:
            ls = None
            gs = None
        else:
            ls = [sum(self.loss(x[i], y[i]) for i in range(self.inputs)) for x, y in zip(xs, ys)]
            if alpha is None:
                gs = None
            else:
                gs = [[self.loss_gradient(xi, yi) for xi, yi in zip(x, y)] for x, y in zip(xs, ys)]
        return yhats, ls, gs
